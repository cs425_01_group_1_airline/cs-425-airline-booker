const queries = {}

queries.buildFlightQuery = (from, to, date, time) =>
	`
	with seqs as (
		SELECT	
			code, ARRAY [
				(code, flight_num, airport_depart, airport_arrive, 
				time_depart_utc, time_arrive_utc, days_transcend,
				getArriveUTC(
					CAST (EXTRACT(epoch from date '${date}') AS integer), 
					time_depart_utc, time_arrive_utc, days_transcend), 1
				)::flight_routine_object
			] as sequence
		FROM flight_routine
		WHERE 
				airport_depart = '${from}' 
			and time_depart_utc >= ${time} 
			and isCloser(airport_depart, airport_arrive, '${to}')),

	seqs2 as (
		(
		SELECT seqs.code as code, array_append(sequence,
			(flight_routine.code, 
			flight_routine.flight_num, 
			flight_routine.airport_depart, 
			flight_routine.airport_arrive, 
			flight_routine.time_depart_utc, 
			flight_routine.time_arrive_utc, 
			flight_routine.days_transcend,
			getArriveUTC(
				sequence[array_upper(sequence, 1)].arrive_utc, 
				flight_routine.time_depart_utc, 
				flight_routine.time_arrive_utc, 
				flight_routine.days_transcend), 2
			)::flight_routine_object) as sequence
		FROM seqs, flight_routine
		WHERE
				array_length(sequence, 1) = 1
			and flight_routine.airport_depart != '${to}'
			and sequence[array_upper(sequence, 1)].airport_arrive = flight_routine.airport_depart
			and isCloser(flight_routine.airport_depart, flight_routine.airport_arrive, '${to}')
		) UNION (
			SELECT code, sequence
			FROM seqs
			WHERE sequence[array_upper(sequence, 1)].airport_arrive = '${to}'
		)
	),

	seqs3 as (
		(
		SELECT seqs.code as code, array_append(sequence,
			(flight_routine.code, 
			flight_routine.flight_num, 
			flight_routine.airport_depart, 
			flight_routine.airport_arrive, 
			flight_routine.time_depart_utc, 
			flight_routine.time_arrive_utc, 
			flight_routine.days_transcend,
			getArriveUTC(
				sequence[array_upper(sequence, 1)].arrive_utc, 
				flight_routine.time_depart_utc, 
				flight_routine.time_arrive_utc, 
				flight_routine.days_transcend), 3
			)::flight_routine_object) as sequence
		FROM seqs2 as seqs, flight_routine
		WHERE
				array_length(sequence, 1) = 2
			and flight_routine.airport_depart != '${to}'
			and sequence[array_upper(sequence, 1)].airport_arrive = flight_routine.airport_depart
			and isCloser(flight_routine.airport_depart, flight_routine.airport_arrive, '${to}')
		) UNION (
			SELECT code, sequence
			FROM seqs2
			WHERE sequence[array_upper(sequence, 1)].airport_arrive = '${to}'
		)
	),

	seqs4 as (
		(
		SELECT seqs.code as code, array_append(sequence,
			(flight_routine.code, 
			flight_routine.flight_num, 
			flight_routine.airport_depart, 
			flight_routine.airport_arrive, 
			flight_routine.time_depart_utc, 
			flight_routine.time_arrive_utc, 
			flight_routine.days_transcend,
			getArriveUTC(
				sequence[array_upper(sequence, 1)].arrive_utc, 
				flight_routine.time_depart_utc, 
				flight_routine.time_arrive_utc, 
				flight_routine.days_transcend), 4
			)::flight_routine_object) as sequence
		FROM seqs3 as seqs, flight_routine
		WHERE
				array_length(sequence, 1) = 3
			and flight_routine.airport_depart != '${to}'
			and sequence[array_upper(sequence, 1)].airport_arrive = flight_routine.airport_depart
			and flight_routine.airport_arrive = '${to}'
		) UNION (
			SELECT code, sequence
			FROM seqs3
			WHERE sequence[array_upper(sequence, 1)].airport_arrive = '${to}'
		)
	),
	list as (
		SELECT row_number, (info).num as index, code, info
		FROM (
			select row_number, code, unnest(sequence) as info 
				from ((
				SELECT ROW_NUMBER() OVER(), *
				FROM seqs4
			)) as temp1) as temp2
	),
	row_numbers as (select distinct row_number from list)

	SELECT flights FROM (
		SELECT temp.row_number, array_agg(info) as flights
		FROM row_numbers inner join ( 
				SELECT * 
				FROM list 
				ORDER BY row_number ASC, (info).num ASC
			) as temp on temp.row_number = row_numbers.row_number
		GROUP BY temp.row_number
		ORDER BY temp.row_number ASC
	) as temp;`

module.exports = queries;