require('dotenv').config()

const moment = require('moment-timezone')
const express = require('express')
const morgan = require('morgan')
const cors = require('cors')
const axios = require('axios')
const db    = require('./db.js');

const suggest = require('./suggest.js');
const search = require('./search.js');

const app = express();

const PORT = 4000;

const airportIATA = db.airportIATA;
const tzoffsets = db.tzoffsets;

app.use(express.json())
app.use(morgan('dev'))
app.use(cors())

app.get('/', (req, res) => {
	res.json({success: true})
})

app.get('/api/suggest', (req, res) => {
	const query = req.query.q;
	if (query) {
		res.json({ suggestions: suggest(query) });
	} else {
		res.json({ result: []});
	}
});

app.get('/api/search', async (req, res) => {

	// 16 or 24 character length queries only
	let q = req.query.q;
	if (!q || (q.length !== 16 && q.length !== 24)) {
		res.json({flights: []});
		return;
	};

	// TODO: add type checking
	let codea = q.substring(0, 3);
	let codeb = q.substring(3, 6);
	let datea = q.substring(10, 14) + "-" + q.substring(6, 8) + "-" + q.substring(8, 10);
	let first = q.substring(14, 15);
	let round = q.substring(15, 16);
	let dateb;

	if (!airportIATA[codea] || !airportIATA[codeb]) {
		res.json({flights: []});
		return;
	}

	let sort = req.query.sort ? parseInt(req.query.sort) : 0;
	if (sort !== 0 && sort !== 1 && sort !== 2) sort = 0;

	let limit = req.query.limit ? parseInt(req.query.limit) : 30;
	if (limit > 30 || limit < 1) limit = 30;

	if (q.length == 24) dateb = q.substring(20, 24) + "-" + q.substring(16, 18) + "-" + q.substring(18, 20);

	let depart_utc_seconds = -tzoffsets[airportIATA[codea].locationData.timecode]*60 % 86400;
	let depart_utc_date    = moment
		.tz(datea + " 00:00", airportIATA[codea].locationData.timecode)
		.tz('UTC').format("YYYY-MM-DD");

	let return_utc_seconds
	let return_utc_date

	if (dateb) {
		return_utc_seconds = -tzoffsets[airportIATA[codeb].locationData.timecode]*60 % 86400;
		return_utc_date    = moment
		.tz(dateb + " 00:00", airportIATA[codeb].locationData.timecode)
		.tz('UTC').format("YYYY-MM-DD");
	}

	console.log(q);

	console.log({
		a: codea.toUpperCase(),
		b: codeb.toUpperCase(),
		time_depart: depart_utc_seconds,
		time_return: return_utc_seconds,
		date_depart: datea,
		date_return: dateb,
		is_roundtrip: round === "1",
		is_firstclass: first === "1",
		sort: sort,
		limit: limit
	})

	const results = await search({
		a: codea.toUpperCase(),
		b: codeb.toUpperCase(),
		time_depart: depart_utc_seconds,
		time_return: return_utc_seconds,
		date_depart: datea,
		date_return: dateb,
		is_roundtrip: round === "1",
		is_firstclass: first === "1",
		sort: sort,
		limit: limit
	});
	
	res.json({
		results: results
	});

	//http://localhost:4000/api/search?q=ordlos120120191112302019
});

app.listen(PORT, () => {
	console.log('Listening on port ' + PORT)
});