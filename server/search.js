const moment = require('moment-timezone');
const db     = require('./db');

const buildFlightQuery = require('./query.js').buildFlightQuery;

const basic_query = db.basic_query;

// buildFlightQuery

const parseResults = rows => {
	rows.forEach(row => {
		let stops = row.substirng(1, row.length - 1).split(",");
	})
}

module.exports = async (query) => {
	
	const a = query.a;
	const b = query.b;
	const date_depart = query.date_depart;
	const date_return = query.date_return;
	const time_depart = query.time_depart;
	const time_return = query.time_return;
	const is_roundtrip = query.is_roundtrip;
	const is_firstclass = query.is_firstclass;
	const sort = query.sort;
	const limit = query.limit;

	try {
		const query_str_nonstop = `
			WITH routines AS (
				SELECT * FROM flight_routine
				WHERE airport_depart = '${a}' and airport_arrive = '${b}' and time_depart_utc >= ${time_depart} 
			)
			SELECT * FROM routines natural join flight
			WHERE date = '${date_depart}';`;
		
		let rows = (await basic_query(buildFlightQuery(a, b, date_depart, time_depart), true)).rows;
		let results = [];

		console.log(rows);
		
		rows.forEach(row => {
			let add = db.airportIATA[row.airport_depart];
			let aad = db.airportIATA[row.airport_arrive];
			let ldd = add.locationData;
			let lad = aad.locationData;
			let dtz = db.tzoffsets[ldd.timecode]*60;
			let atz = db.tzoffsets[lad.timecode]*60;

			row.date = moment.unix(
				row.date.getTime()/1000 - 
				row.date.getTimezoneOffset()*60
			).utc().format("YYYY-MM-DD");
			
			row.airport_depart = {
				iata: add.iata,
				name: add.name,
				location: {
					country: ldd.country,
					city: ldd.city,
					state: ldd.state,
					timezone: dtz
				}
			};

			row.airport_arrive = {
				iata: aad.iata,
				name: aad.name,
				location: {
					country: lad.country,
					city: lad.city,
					state: lad.state,
					timezone: atz
				}
			};

			row.price_first   = parseFloat(row.price_first);
			row.price_economy = parseFloat(row.price_economy);

			/*
				const result = {
					airline: {
						code: 'AA',
						name: 'American Airlines'
					},
					departure: {
						date: Date.now(),
						stops: [
							{ routine: {
									number: 420, from: "ORD", to: "IAD",
									seats: { economy: 560, first: 140 },
									time: { departure: 30600, arrival: 45000, days_transcend: 0 } },
								flight: { 
									date: Date.now(), 
									ordered: { economy: 529, first: 111 },
									price: { economy: 213, first: 655 } }
							},
							{ routine: {
									number: 420, from: "IAD", to: "PEK",
									seats: { economy: 560, first: 140 },
									time: { departure: 3600*14, arrival: 3600, days_transcend: 1 } },
								flight: {
									date: Date.now(),
									ordered: { economy: 529, first: 111 },
									price: { economy: 657, first: 1382 } }
							}]},
					returning: {
						date: Date.now() + 1000*60*60*24*2,
						stops: [
							{ routine: {
									number: 420, from: "PEK", to: "NYC",
									seats: { economy: 560, first: 140 },
									time: { departure: 3600*14, arrival: 3600, days_transcend: 1 } },
								flight: {
									date: Date.now(),
									ordered: { economy: 529, first: 111 },
									price: { economy: 657, first: 1382 } }
							},
							{ routine: {
								number: 420, from: "NYC", to: "ORD",
								seats: { economy: 560, first: 140 },
								time: { departure: 30600, arrival: 45000, days_transcend: 0 } },
							flight: { 
								date: Date.now(), 
								ordered: { economy: 529, first: 111 },
								price: { economy: 213, first: 655 } }
							}]},
				}
			*/
			console.log(row.code);
			console.log(db.airlines);
			results.push({
				airline: {
					code: row.code,
					name: db.airlines[row.code].name,
				}, 
				departure: [row]
			});
		})

		console.log(results);
		return results
		
	} catch(e) {
		console.error(e);
		return [];
	}

}