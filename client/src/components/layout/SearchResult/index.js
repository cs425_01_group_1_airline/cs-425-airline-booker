import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import { Button } from '@material-ui/core';
import { FlightLand, FlightTakeoff, MoreVert } from '@material-ui/icons';

import './style.scss';
import { getDurations, formatDuration, formatTime, UTCtoLocalDate } from '../../../utils/time';

// returns the total price of all stops for economy and first class
const getPrices = stops => {
	let price_economy = 0;
	let price_first = 0;

	stops.forEach(stop => {
		price_economy += stop.price_economy;
		price_first   += stop.price_first;
	});

	return { price_economy, price_first };
}


// renders a search result
export default function SearchResult(props) {
	
	const { result, isFirst } = props;
	//const { returning } = result;

	const airline = result.airline;
	const stops   = result.departure;

	const { dur_total, dur_flight, dur_layover, 
		      stop_dur_flights, stop_dur_layovers } = getDurations(stops);
	const { price_economy, price_first } = getPrices(stops);

	const stop_first = stops[0];
	const stop_last  = stops[stops.length - 1];

	const bookFirst   = (
		<div key="bookfirst" className="search-result-book">
			<Button className={"search-result-book-button " + (isFirst ? "primary" : "secondary")} >
				Book First Class
			</Button>
		</div>
	);

	const bookEconomy = (
		<div key="bookeconomy" className="search-result-book">
			<Button className={"search-result-book-button " + (isFirst ? "secondary" : "primary")}>
				Book Economy
			</Button>
		</div>
	);

	let strflightcodes = "";

	stops.forEach(stop => strflightcodes += " · " + airline.code + stop_first.flight_num);

  return (
    <div className="search-result">
      <ExpansionPanel>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
					<div className="data-set">
						<div className="data-set-1">
							<Typography  variant="h6"> {
								formatTime(stop_first.time_depart_utc + stop_first.airport_depart.location.timezone) + " — " +
								formatTime(stop_last .time_arrive_utc + stop_last .airport_arrive.location.timezone) }
							</Typography>
							<Typography variant="overline"> {
								airline.name + strflightcodes }
							</Typography>
						</div>
						<div className="data-set-2">
							<Typography variant="subtitle1"> 
								{ formatDuration(dur_total) }
							</Typography>
							<Typography variant="overline"> { 
								stop_first.airport_depart.iata + " 🠒 " + stop_last.airport_arrive.iata }
							</Typography>
						</div>
						<div className="data-set-3">
							<Typography> { 
								stops.length == 1 ? "Nonstop" : stops.length - 1 + " stop" }
							</Typography>
							{ stops.length != 1 ?
								<Typography variant="overline"> { 
									formatDuration(dur_layover) + " LAY" }
								</Typography> : undefined
							}
						</div>
						<div className="data-set-4">
							<Typography className="search-result-price" variant="button"> {
								"$" + (isFirst ? price_first : price_economy) } 
							</Typography>
							<Typography variant="overline" className="search-result-seat-type"> {
								isFirst ? "First Class" : "Economy" } 
							</Typography>
						</div>
						<div className="data-set-5">
							<Typography className="search-result-price secondary" variant="button"> {
								"$" + (isFirst ? price_economy : price_first) } 
							</Typography>
							<Typography variant="overline" className="search-result-seat-type"> {
								isFirst ? "Economy" : "First Class" } 
							</Typography>
						</div>
					</div>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
					<div className="search-result-stops">
						{
							<div className="layover-details-container flight-type-container">
								<div className="airport-details-info flight-type">
									<Typography variant="h6" className="airport-details-time" display="inline"> 
										{ "Departure" } 
									</Typography>
									<div className="airport-details-name-iata">
										<Typography variant="subtitle1" className="airport-details-name"> 
											{ UTCtoLocalDate (
												stop_first.date, 
												stop_first.time_depart_utc, 
												stop_first.airport_depart.location.timezone
											) } 
										</Typography>
										<Typography variant="overline"  className="airport-details-iata"> {  } </Typography>
									</div>
								</div>
							</div>
						}
						{ stops.map((stop, index) => 
								<StopInfo
									key = {index.toString()}
									airline={airline} 
									stop={stop} 
									index={index} 
									flightDurations={stop_dur_flights} 
									layoverDurations={stop_dur_layovers}
									isFirst={isFirst}
								/>) 
						}
					</div>
          <div className="search-result-book-container">
						{ isFirst ? [bookFirst, bookEconomy] : [bookEconomy, bookFirst] }
					</div>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    </div>
  );
}

// renders flight information between airports
class StopInfo extends React.Component {
	render() {
		const { stop, index, flightDurations, layoverDurations, isFirst } = this.props;
		const durationFlight  = flightDurations[index];
		const durationLayover = index && layoverDurations[index];
		
		return (
			<div className="stop-info">
				{	index ?
					<div className="layover-details-container">
						<Typography variant="button" className="layover-details"> 
							{ "Layover duration: " + formatDuration(durationLayover, true) } 
						</Typography>
					</div> : undefined
				}
				<div className="stop-info-flight">
					<div className="stop-info-flight-left">
						<AirportDetails 
							airportName = { stop.airport_depart.name }
							airportIATA = { stop.airport_depart.iata }
							departTime  = { formatTime(stop.time_depart_utc + stop.airport_depart.location.timezone) } 
						/>
						<div className="airport-details">
							<div className="airport-details-icon-container">
								<MoreVert 
									htmlColor="#A9A9A9" 
									shapeRendering="geometricPrecision" 
									fontSize="large" 
									className="airport-details-icon"
								/>
							</div>
							<div className="airport-details-info">
								<Typography variant="button" className="stop-info-duration"> 
									{ "Duration: " + formatDuration(durationFlight, true) }
								</Typography>
							</div>
						</div>
						<AirportDetails 
							isLanding
							airportName = { stop.airport_arrive.name }
							airportIATA = { stop.airport_arrive.iata }
							departTime  = { formatTime(stop.time_arrive_utc + stop.airport_arrive.location.timezone) } 
						/>
					</div>
					<div className="stop-info-flight-right">
						<div className="price-container">
							<Typography className="price seat-primary" variant="button"> {
								"$" + (isFirst ? stop.price_first : stop.price_economy) } 
							</Typography>
							<Typography variant="overline" className="seat-type"> {
								isFirst ? "First Class" : "Economy" } 
							</Typography>
						</div>
						<div className="price-container">
							<Typography className="price seat-secondary" variant="button"> {
								"$" + (isFirst ? stop.price_economy : stop.price_first) } 
							</Typography>
							<Typography variant="overline" className="seat-type"> {
								isFirst ? "Economy" : "First Class" } 
							</Typography>
						</div> 
					</div>
				</div>				
			</div>
		)
	}
} 

// renders information about a particular airport
class AirportDetails extends React.Component {
	render() {
		const { airportName, airportIATA, departTime, isLanding } = this.props;

		return (
			<div className="airport-details">
				<div className="airport-details-icon-container">
					{ isLanding ? 
						<FlightLand    
							htmlColor="#3C4043" shapeRendering="geometricPrecision" 
							fontSize="large"    className="airport-details-icon"/> :
						<FlightTakeoff 
							htmlColor="#3C4043" shapeRendering="geometricPrecision" 
							fontSize="large"    className="airport-details-icon"/>
					}
				</div>
				<div className="airport-details-info">
					<Typography variant="h6" className="airport-details-time" display="inline"> 
						{ departTime } 
					</Typography>
					<div className="airport-details-name-iata">
						<Typography variant="subtitle1" className="airport-details-name"> { airportName } </Typography>
						<Typography variant="overline"  className="airport-details-iata"> { airportIATA } </Typography>
					</div>
				</div>
			</div>
		);
	}
}