import React from 'react';
import MenuIcon from '@material-ui/icons/Menu';
import { AppBar, Toolbar, IconButton, Typography, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
	bar: {
		position: 'fixed',
		backgroundColor: 'rgba(255, 255, 255, 0)',
		boxShadow: 'none',
		top: 0,
		left: 0,
	},
  root: {
    flexGrow: 1,
  },
  menuButton: {
		marginRight: theme.spacing(2),
		color: "white",
		textDecoration: "none"
	},
	signupButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

export default function TopBar() {
	
	const classes = useStyles();

	return (
		<AppBar position="static" className={classes.bar}>
			<Toolbar>
				<IconButton edge="start" className={classes.menuButton} color="primary" aria-label="menu">
					<MenuIcon />
				</IconButton>
				<Typography variant="h6" className={classes.title}/>
				<Button color="primary" className={classes.menuButton}>Sign Up</Button>
				<Link to="/login">
					<Button color="primary" style={{color: "white", textDecoration: "none"}}>Login</Button>
				</Link>
			</Toolbar>
		</AppBar>
	);
}

