import React from 'react';
import suggest from '../../../utils/search';
import Autosuggest from 'react-autosuggest';

// material-ui styles + custom
import './style.scss';

// material-ui components
import {
	List, ListItem, ListItemIcon,
	TextField, Typography, InputAdornment
} from '@material-ui/core';

// material-ui assets
import { Room, GpsFixed, LocationCity, LocalAirport } from '@material-ui/icons';

//------------------------------------//

// getters for rendering
const getSuggestionValue    = suggestion => suggestion.location;
const getSectionSuggestions = section    => section.suggestions;

// returns a render of a suggestion
const renderSuggestion      = suggestion => {
	return (<SearchResult suggestion={suggestion} />);
}

// returns a render of a section title
const renderSectionTitle = section => {
	if (section.suggestions.length < 2) return <div/>;
	return (
		<ListItem disableRipple className="airport-search-result airport-search-result-section">
			<ListItemIcon>
				<LocationCity htmlColor="#3C4043" shapeRendering="crispEdges"/>
			</ListItemIcon>
			<div>
				<Typography variant="subtitle1" className="airport-search-result-title">
					{ section.location }
				</Typography>
				<Typography variant="body2" className="airport-search-result-iata">
					Airports
				</Typography>
			</div>
		</ListItem>
	);
}

// returns a render of the suggestions container
const renderSuggestionsContainer = ({containerProps, children, query}) => {
	return (
		<List component="div" disablePadding {...containerProps}>
			{children}
		</List>
	);
}

// returns a render of the input component
const renderInputComponent = inputProps => {
	const { ref, className, label, ...rest } = inputProps;
  return (
    <TextField
			id="outlined-basic"
			fullWidth
			label={label || "Label"}
			margin="normal"
			variant="outlined"
			className= {(className || "" ) + " airport-search-input"}
      InputProps={{ 
				inputRef: node => ref(node),
				startAdornment: (
					<InputAdornment position="start">
						<Room htmlColor="#3C4043"/>
					</InputAdornment>
				),
			 }}
      {...rest}
    />
	);
}

// Search Result Render
class SearchResult extends React.Component {
	render() {
		const { suggestion } = this.props;
		const display = suggestion.display;

		return (
			<ListItem button disableRipple className="airport-search-result">
				<ListItemIcon>
					{  display === "A" ? 
						<LocationCity          
							htmlColor="#3C4043" 
							className={display === "B" && "left-shift"} 
							shapeRendering="crispEdges"/> :
						<LocalAirport 
							htmlColor="#3C4043" 
							className={display === "B" && "left-shift"} 
							shapeRendering="geometricPrecision"/> 
					}
				</ListItemIcon>
				<div>
					<Typography variant="subtitle1" className="airport-search-result-title">
						{ display === "A" ? suggestion.location : suggestion.name }
					</Typography>
					<Typography variant="overline" className="airport-search-result-iata">
						{ suggestion.iata }
					</Typography> 
					{ display !== "B" && 
						<Typography variant="caption" className="airport-search-result-caption">
							{ display  == "A" ? suggestion.name : suggestion.location }
						</Typography> 
					}
				</div>
			</ListItem>
		);
	}
}

// Airport Select Class
export default class AirportSelect extends React.Component {
  constructor() {
    super();

    this.state = {
      value: '',
			suggestions: [],
			airport: null
    };
  }

  onChange = (event, { newValue }) => {
    this.setState({ value: newValue });
  };

  onSuggestionsFetchRequested = async ({ value }) => {
    this.setState({ suggestions: await suggest(value) });
  };

  onSuggestionsClearRequested = () => {
    this.setState({ suggestions: [] });
	};
	
	onSuggestionSelected = (event, { suggestion, suggestionValue, suggestionIndex, sectionIndex, method }) => {
		this.setState({ airport: suggestion });
		if (this.props.onSuggestionSelected) this.props.onSuggestionSelected(suggestion);
	};
	


  render() {
		const { className, label, onSuggestionSelected, ...rest } = this.props;
		const { value, suggestions, airport } = this.state;
    const inputProps = {
      value, label,
			onChange: this.onChange,
    };

    return (
			<div className = {(className || "") + " airport-picker"} { ...rest }>
				<Autosuggest
					multiSection
					
					inputProps                  = {inputProps}
					suggestions                 = {suggestions}

					onSuggestionSelected        = {this.onSuggestionSelected}
					onSuggestionsFetchRequested = {this.onSuggestionsFetchRequested}
					onSuggestionsClearRequested = {this.onSuggestionsClearRequested}
					
					renderSuggestion            = {renderSuggestion}
					renderSectionTitle          = {renderSectionTitle}
					renderInputComponent        = {renderInputComponent}
					renderSuggestionsContainer  = {renderSuggestionsContainer}

					getSuggestionValue          = {getSuggestionValue}
					getSectionSuggestions       = {getSectionSuggestions}

					focusInputOnSuggestionClick = {false}
				/>
				{ airport && <div className="airport-selected-container">
						<GpsFixed htmlColor="#3C4043" className="airport-selected-icon"/>
						<Typography variant="overline" className="airport-selected-caption">
							{ airport.name }
						</Typography>
					</div>
				}
			</div>
    );
  }
}

// SEARCH SUGGESTION FORMATS
// FORMAT A [CITY-SINGLE]
// 		CITY, [STATE OR COUNTRY] (AIRPORT_CODE) // STATE AND COUNTRY FOR CANADA
//		AIRPORT_NAME
// FORMAT B [CITY-MULTI]											// PROCEEDS // CITY, [STATE OR COUNTRY] // HEADER
//		AIRPORT_NAME (AIRPORT_CODE)
// FORMAT C [CODE, NAME]
//		AIRPORT_NAME (AIRPORT_CODE)
//		CITY, [STATE OR COUNTRY]

// IDENTIFIED BY CITY 				// PRIORITY: 2 IF CHARS == 3, 1 OTHERWISE
// CITY-SINGLE	// FORMAT A
// CITY-MULTI		// FORMAT B

// IDENTIFIED BY AIRPORT CODE // PRIORITY: 2 IF CHARS < 3, 1 IF CHARS == 3, 0 OTHERWISE
// CITY-SINGLE	// FORMAT C
// CITY-MULTI		// FORMAT B

// IDENTIFIED BY AIRPORT NAME // PRIORITY: 3 IF CHARS <= 3, 2 OTHERWISE
// CITY-SINGLE	// FORMAT C
// CITY-MULTI		// FORMAT B

// IDENTIFIED BY STATE				// PRIORITY: 3
// CITY-SINGLE	// FORMAT A
// CITY-MULTI		// FORMAT B

// IDENTIFIED BY COUNTRY			// PRIORITY: 4
// CITY-SINGLE	// FORMAT A
// CITY-MULTI		// FORMAT B