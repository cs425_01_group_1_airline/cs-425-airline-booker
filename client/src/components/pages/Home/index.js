import React, { useState } from 'react';
import './style.scss';

import AirportSelect from '../../layout/AirportSelect';
import { Button } from '@material-ui/core';

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { DatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';

export default function Home() {
	
	const [selectedDateDepart, handleDateDepartChange] = useState(new Date());
	const [selectedDateReturn, handleDateReturnChange] = useState(new Date());

  return (
  	<div id="app-home">
			<div id="curve">
				<div id="curve-contents">
					<div id="world-map"/>
				</div>
			</div>
			<div id="direct-search">
				<div id="direct-search-container">
					<div id="direct-search-container-1">
						<AirportSelect label="From"/>
						<AirportSelect label="To"/>
					</div>
					<div id="direct-search-dps">
						<div id="direct-search-dps-container-1">
							<MuiPickersUtilsProvider utils={MomentUtils}>
								<DatePicker className="date-picker"
									disableToolbar
									variant="inline"
									format="MM/DD/YYYY"
									label="Depart"
									value={selectedDateDepart}
									onChange={handleDateDepartChange}
								/>
							</MuiPickersUtilsProvider>
						</div>
						<div id="direct-search-dps-container-2">
							<MuiPickersUtilsProvider utils={MomentUtils}>
								<DatePicker className="date-picker"
									disableToolbar
									variant="inline"
									format="MM/DD/YYYY"
									label="Return"
									value={selectedDateReturn}
									onChange={handleDateReturnChange}
								/>
							</MuiPickersUtilsProvider>
						</div>
						<div id="direct-search-container-2">
							<div>
								<RadioGroup aria-label="gender" name="gender2" row>
									<FormControlLabel
										value="oneway"
										control={<Radio color="primary" />}
										label="One-Way"
										labelPlacement="end"
									/>
									<FormControlLabel
										value="roundtrip"
										control={<Radio color="primary" />}
										label="Round Trip"
										labelPlacement="end"
									/>
								</RadioGroup>
							</div>
							<div className="vertical-line" />
							<div>
								<RadioGroup aria-label="gender2" name="gender3" row>
									<FormControlLabel
										value="economy"
										control={<Radio color="primary" />}
										label="Economy"
										labelPlacement="end"
									/>
									<FormControlLabel
										value="firstclass"
										control={<Radio color="primary" />}
										label="First Class"
										labelPlacement="end"
									/>
								</RadioGroup>
							</div>
						</div>
					</div>
				</div>
				<Button id="direct-search-button">Search</Button>
			</div>
		</div> 
	);
}