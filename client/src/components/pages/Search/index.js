import React, { useState } from 'react';

import './style.scss';
import SearchResult from '../../layout/SearchResult';

import 'react-date-range/dist/styles.css'; // main style file
import 'react-date-range/dist/theme/default.css'; // theme css file

import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import AirportSelect from '../../layout/AirportSelect';
import { Button, Typography, ClickAwayListener } from '@material-ui/core';

import { DateRange as DateRnageIcon, Search as SearchIcon } from '@material-ui/icons';
import { Calendar, DateRangePicker, DateRange } from 'react-date-range';

import { formatDate, formatUTCDate } from '../../../utils/time';

/*
	airline: {
		code: 'DL',
		name: 'Delta Airlines'
	},
	departure: {
		date: Date.now(),
		stops: [
			{ routine: {
					number: 420, from: "ORD", to: "IAD",
					seats: { economy: 560, first: 140 },
					time: { departure: 30600, arrival: 45000, days_transcend: 0 } },
				flight: { 
					date: Date.now(), 
					ordered: { economy: 529, first: 111 },
					price: { economy: 213, first: 655 } }
			},
			{ routine: {
					number: 420, from: "IAD", to: "PEK",
					seats: { economy: 560, first: 140 },
					time: { departure: 3600*14, arrival: 3600, days_transcend: 1 } },
				flight: {
					date: Date.now(),
					ordered: { economy: 529, first: 111 },
					price: { economy: 657, first: 1382 } }
			}]},
	returning: {
		date: Date.now() + 1000*60*60*24*2,
		stops: [
			{ routine: {
					number: 420, from: "PEK", to: "NYC",
					seats: { economy: 560, first: 140 },
					time: { departure: 3600*14, arrival: 3600, days_transcend: 1 } },
				flight: {
					date: Date.now(),
					ordered: { economy: 529, first: 111 },
					price: { economy: 657, first: 1382 } }
			},
			{ routine: {
				number: 420, from: "NYC", to: "ORD",
				seats: { economy: 560, first: 140 },
				time: { departure: 30600, arrival: 45000, days_transcend: 0 } },
			flight: { 
				date: Date.now(), 
				ordered: { economy: 529, first: 111 },
				price: { economy: 213, first: 655 } }
			}]},
*/

const result = {
	airline: {
		code: 'AA',
		name: 'American Airlines'
	},
	departure: {
		date: Date.now(),
		stops: [
			{ routine: {
					number: 420, from: "ORD", to: "IAD",
					seats: { economy: 560, first: 140 },
					time: { departure: 30600, arrival: 45000, days_transcend: 0 } },
				flight: { 
					date: Date.now(), 
					ordered: { economy: 529, first: 111 },
					price: { economy: 213, first: 655 } }
			},
			{ routine: {
					number: 420, from: "IAD", to: "PEK",
					seats: { economy: 560, first: 140 },
					time: { departure: 3600*14, arrival: 3600, days_transcend: 1 } },
				flight: {
					date: Date.now(),
					ordered: { economy: 529, first: 111 },
					price: { economy: 657, first: 1382 } }
			}]},
	returning: {
		date: Date.now() + 1000*60*60*24*2,
		stops: [
			{ routine: {
					number: 420, from: "PEK", to: "NYC",
					seats: { economy: 560, first: 140 },
					time: { departure: 3600*14, arrival: 3600, days_transcend: 1 } },
				flight: {
					date: Date.now(),
					ordered: { economy: 529, first: 111 },
					price: { economy: 657, first: 1382 } }
			},
			{ routine: {
				number: 420, from: "NYC", to: "ORD",
				seats: { economy: 560, first: 140 },
				time: { departure: 30600, arrival: 45000, days_transcend: 0 } },
			flight: { 
				date: Date.now(), 
				ordered: { economy: 529, first: 111 },
				price: { economy: 213, first: 655 } }
			}]},
}

const fmn = (num) => {
	return num < 10 ? "0" + num.toString() : num.toString();
}

let last_time_clicked = Date.now() - 300;

export default function Search() {

	const ddd = new Date();
	const ddr = new Date();
	ddr.setDate(ddr.getDate() + 1);

	const [dateDepart, handleDateDepartChange] = useState(ddd);
	const [dateReturn, handleDateReturnChange] = useState(ddr);
	
	if (!dateReturn) dateReturn.setDate(dateDepart.getDate() + 1);

	const [airportDepart, handleAirportDepartChange] = useState();
	const [airportArrive, handleAirportArriveChange] = useState();

	const [typeClass, handleClassChange] = useState("economy");
	const [typeTrip , handleTripChange ] = useState("oneway");

	const [results, handleResultsChange] = useState([]);

	const [datePickerOpen, handleDatePickerOpenChange] = useState(false);

	const handleDateChanges = (start, end) => {
		const unxa = start.getTime();
		const unxb =   end.getTime();

		const cstart = new Date(unxa);
		const cend   = new Date(unxb);

		const MS_DAY = 1000*60*60*24;

		if (unxb - unxa < MS_DAY) {
			cend.setTime(unxa + MS_DAY);
		}

		if (unxa > dateReturn.getTime()) {
			handleDateReturnChange(cend);
			handleDateDepartChange(cstart);
		} else {
			handleDateDepartChange(cstart);
			handleDateReturnChange(cend);
		}
	}

	const onDatePickerClick = () => {
		last_time_clicked = Date.now();
		handleDatePickerOpenChange(!datePickerOpen);
	}

	const onDatePickerClickAway = () => {
		if (datePickerOpen && Date.now() - last_time_clicked > 300) handleDatePickerOpenChange(false);
	}

	const onSubmit = async () => {
		if (!airportArrive || !airportDepart) return;
		console.log(dateDepart, dateReturn, airportDepart, airportArrive, typeClass, typeTrip);
		
		const dm = dateDepart.getUTCMonth() + 1;
		const dd = dateDepart.getUTCDate();
		const dy = dateDepart.getUTCFullYear();

		const rm = dateReturn && dateReturn.getUTCMonth() + 1;
		const rd = dateReturn && dateReturn.getUTCDate();
		const ry = dateReturn && dateReturn.getUTCFullYear();

		const query = airportDepart.toLowerCase() + airportArrive.toLowerCase() 
			+	fmn(dm) + fmn(dd) + fmn(dy)
			+ (typeClass === "economy" ? "0" : "1")
			+ (typeTrip  === "oneway"  ? "0" : "1")
			+ (typeTrip  === "oneway"  ? ""  : (fmn(rm) + fmn(rd) + fmn(ry)));

		// console.log("http://localhost:4000/api/search?q=" + query);

		try {
			const result = await (await fetch("http://localhost:4000/api/search?q=" + query)).json();
			handleResultsChange(result ? (result.results || []) : []);
			console.log(result ? (result.results || []) : []);
		} catch (e) {
			console.error(e);
			return [];
		}
	}

	console.log(typeClass !== "economy");
	console.log(typeClass)

	return (
		<div id="app-search">
			<div id="search-tab">
				<div id="search-tab-selects">
					<FormControl>
						<Select
							id="select-trip"
							className="search-tab-select"
							value={typeTrip}
							onChange={(event) => handleTripChange(event.target.value)}
						>
							<MenuItem value="oneway">One-way</MenuItem>
							<MenuItem value="roundtrip">Round Trip</MenuItem>
						</Select>
					</FormControl>
					<FormControl>
						<Select
							id="select-class"
							className="search-tab-select"
							value={typeClass}
							onChange={(event) => handleClassChange(event.target.value)}
						>
							<MenuItem value="economy">Economy</MenuItem>
							<MenuItem value="first">First Class</MenuItem>
						</Select>
					</FormControl>
				</div>
				<div id="search-tab-inputs">
					<AirportSelect label="From" onSuggestionSelected={suggestion => handleAirportDepartChange(suggestion.iata)}/>
					<AirportSelect label="To"   onSuggestionSelected={suggestion => handleAirportArriveChange(suggestion.iata)}/>
					<Button disableRipple id="search-tab-datepicker" onClick={onDatePickerClick}>
						<DateRnageIcon
							htmlColor="#3C4043" 
							id="datepicker-icon" 
							shapeRendering="geometricPrecision" 
						/>
						<Typography id="datepicker-depart" className="datepicker-text" variant="button"> 
							{ formatDate(dateDepart) } 
						</Typography>
						{ typeTrip === "oneway" ? undefined :
							[
								<div key="1" id="datepicker-divider"/>,
								<Typography key="2" id="datepicker-return" className="datepicker-text" variant="button">
									{ dateReturn ? formatDate(dateReturn) : "" } 
								</Typography>
							]
						}
					</Button>
					
					<ClickAwayListener onClickAway={onDatePickerClickAway}>
						
						{ typeTrip === "oneway" ?
							<Calendar
								className={"datepicker-calender " + (datePickerOpen ? "open" : "")}
								date={dateDepart}
								onChange={(startDate) => {
									handleDateChanges(startDate, dateReturn);
								}}
								showMonthAndYearPickers={false}
								minDate={new Date()}
							/>
							:
							<DateRange
								className={"datepicker-calender " + (datePickerOpen ? "open" : "")}
								showMonthAndYearPickers={false}
								minDate={new Date()}
								showDateDisplay={false}
								ranges={[{
									startDate: dateDepart,
									endDate: dateReturn,
									key: "selection"
								}]}
								onChange={(ranges) => {
									let { startDate, endDate } = ranges.selection;
									handleDateChanges(startDate, endDate);
								}}
							/>
						}
						
					</ClickAwayListener>
					
					<Button id="search-button" onClick={onSubmit}>
						<SearchIcon
							htmlColor="#FFFFFF" 
							id="search-button-icon"
						/>
					</Button>
				</div>
			</div>
			<div id="app-search-result">
					{ results.map((result, index) => <SearchResult key={"search-result-" + index} result={result} isFirst={typeClass !== "economy"}/>) }
			</div>
		</div>
	);
}