import Search from "../components/pages/Search";

export default async query => {
	console.log(query);
	if (!query) return [];
	try {
		const result = await (await fetch('http://localhost:4000/api/suggest?q=' + query)).json();
		return result ? (result.suggestions || []) : [];
	} catch (e) {
		console.error(e);
		return [];
	}
}