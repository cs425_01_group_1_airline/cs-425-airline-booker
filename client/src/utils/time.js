
const SECONDS_MIN  = 60
const SECONDS_HOUR = SECONDS_MIN *60;
const SECONDS_DAY  = SECONDS_HOUR*24;

// how to actually perform a modulus operation, stupid javascript
const mod = (n, m) => ((n % m) + m) % m;

// turns takes all stop data and returns
//		total duration
//		total flight duration
//		total layover duration
//		flight duration of each stop
//		layover duration between each stop

const WEEKDAYS = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
const MONTHS   = [
	"January", "Feburary", "March", "April", 
	"May", "June", "July", "August", 
	"September", "October", "November", "December"];

const formatUTCDate = date => {
	return WEEKDAYS[date.getUTCDay()].substring(0, 3) + ", " + 
	       MONTHS[date.getUTCMonth()].substring(0, 3) + " " + date.getUTCDate();
}

const formatDate = date => {
	return WEEKDAYS[date.getDay()].substring(0, 3) + ", " + 
	       MONTHS[date.getMonth()].substring(0, 3) + " " + date.getDate();
}

const UTCtoLocalDate = (utc_date, utc_time, tzoffset_s) => {
	return formatUTCDate(new Date(
		Date.UTC(
			parseInt(utc_date.substring( 0,  4)), 
			parseInt(utc_date.substring( 5,  7)) - 1, 
			parseInt(utc_date.substring( 8, 10)) 
		) + utc_time*1000 + tzoffset_s*1000
	));
}

const getDurations = stops => {
	let dur_flight = 0;
	let dur_layover = 0;

	let stop_dur_flights  = [];
	let stop_dur_layovers = [];

	let last_time;

	stops.forEach((stop, i) => {
		const { time_depart_utc, time_arrive_utc, days_transcend } = stop;

		const stop_dur_flight = days_transcend ? 
			SECONDS_DAY - time_depart_utc + SECONDS_DAY*(days_transcend - 1) + time_arrive_utc :
			time_arrive_utc - time_depart_utc;

		dur_flight += stop_dur_flight;
		stop_dur_flights[i] = stop_dur_flight
		
		if (i !== 0) { 
			const stop_dur_layover = time_depart_utc < last_time ? 
				SECONDS_DAY - last_time + time_depart_utc : 
				time_depart_utc - last_time;

			dur_layover += stop_dur_layover;
			stop_dur_layovers[i] = stop_dur_layover;
		}

		last_time = time_arrive_utc;
	});

	return {
		stop_dur_flights, stop_dur_layovers, dur_flight , dur_layover,
		dur_total: dur_flight + dur_layover 
	};
}

// converts seconds to HH:MM AM/PM format
const formatTime = seconds => {

	seconds = mod(seconds, SECONDS_DAY);
	
	let time_hour = (Math.floor(seconds/SECONDS_HOUR) - 1) % 12 + 1;
	let time_min = Math.floor(seconds % SECONDS_HOUR / SECONDS_MIN);
	return  (time_hour < 10 ? (time_hour == 0 ? 12 :"0" + time_hour) : time_hour) + ":" +
					(time_min < 10 ? "0" + time_min : time_min) +
					(seconds >= SECONDS_HOUR*12 ? " PM" : " AM");
}

// converts seconds to HH:MM format
const formatDuration = (seconds, pad) => {

	seconds = mod(seconds, SECONDS_DAY);

	let time_hour = (Math.floor(seconds/SECONDS_HOUR) - 1) % 12 + 1;
	let time_min = Math.floor(seconds % SECONDS_HOUR / SECONDS_MIN);
	
	if (pad) {
		return (time_hour < 10 ? "0" + time_hour : time_hour) + "h " +
		       (time_min  < 10 ? "0" + time_min  : time_min ) + "m";
	} else return time_hour + "h " + time_min + "m";
}

export { getDurations, formatDuration, formatTime, formatDate, formatUTCDate, UTCtoLocalDate }