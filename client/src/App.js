import React, { useState } from 'react';

// top bar
import TopBar from './components/layout/TopBar';

// pages
import Home from './components/pages/Home';
import SignIn from './components/pages/SignIn';
import Search from './components/pages/Search';

// routing
import { Route, BrowserRouter as Router, withRouter } from 'react-router-dom';

// global styles
import './App.scss';

export default function App() {
  return (
		<Router>
			<TopBar/>
			<div id="app-root">
				<div>
					<Route exact path="/lol" component={Home}/>
					<Route exact path="/login" component={SignIn}/>
					<Route exact path="/" component={Search}/>
				</div>
			</div> 
		</Router>
	);
}
