DROP SCHEMA public CASCADE;
CREATE SCHEMA public;

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

create table location (
	country  varchar(90),
	city     varchar(90),
	state    varchar(90),
	timecode varchar(90),
	
	primary key (country, city)
);

create table airport (
	IATA    char(3) unique,
	name    varchar(90),
	country varchar(90),
	city    varchar(90),
	
	primary key (IATA),
	foreign key (country, city) references location (country, city)
		on delete cascade
);

create table customer (
	customer_ID     uuid default uuid_generate_v4(),
	first_name      varchar(50) not null,
	middle_name     varchar(50),
	last_name       varchar(50) not null,
	home_airport    char(3),
	email_address   varchar(50) unique,
	password_hash   varchar(256),
	
	primary key (customer_ID),
	foreign key (home_airport) references airport (IATA)
		on delete set null 
);

create table customer_address (
  address_ID      uuid,
	customer_ID	    uuid,
	address         varchar(90),
	city            varchar(90),
	province        varchar(90),
	country         varchar(90),
	
	primary key (address_ID),
	foreign key (customer_ID) references customer (customer_ID)
		on delete restrict
);

create table customer_card (
	customer_ID     uuid,
	address_ID      uuid,
	card_number     char(16),
	expire_month    integer   check (expire_month > 0 and expire_month <= 12),
	expire_year     integer   check (expire_year  > 2019),
	
	primary key (customer_ID, card_number),
	foreign key (customer_ID) references customer         (customer_ID)
		on delete cascade,
	foreign key (address_ID)  references customer_address (address_ID)
		on delete restrict
);

create table airline (	
	code    char(2),
	name    varchar(90),
	country varchar(90),

	primary key (code)
);

create table flight_routine (
	code                char(2),
	flight_num          integer check (flight_num >= 0),
	airport_depart      char(3),
	airport_arrive      char(3),
	time_depart_utc	    integer check (time_depart_utc >= 0 and time_depart_utc < 86400),
	time_arrive_utc     integer check (time_arrive_utc >= 0 and time_arrive_utc < 86400),
	days_transcend	    integer check (days_transcend  >= 0),
	
	primary key (code, flight_num),
	foreign key (code)           references airline(code)
		on delete cascade,
	foreign key (airport_depart) references airport(IATA)
		on delete cascade,
	foreign key (airport_arrive) references airport(IATA)
		on delete cascade
);

create table flight (
	date	               date,
	code	               char(2),
	flight_num           integer check (flight_num >= 0),
	seat_ordered_first   integer check (seat_ordered_first   >= 0 and seat_ordered_first   <= seats_num_first),
	seat_ordered_economy integer check (seat_ordered_economy >= 0 and seat_ordered_economy <= seats_num_economy),
	seats_num_first      integer check (seats_num_first      >= 0),
	seats_num_economy    integer check (seats_num_economy    >= 0),
	price_first          numeric(8, 2)  check (price_first   > 0),
	price_economy	       numeric(8, 2)  check (price_economy > 0 and price_economy < price_first),
	
	primary key (code, flight_num, date),
	foreign key (code, flight_num) references flight_routine(code, flight_num)
		on delete cascade
);

create table distance (
	airport_one  char(3),
	airport_two  char(3),
	distance     integer check (distance > 0),
	primary key (airport_one, airport_two),
	foreign key (airport_one) references airport(IATA)
		on delete cascade,
	foreign key (airport_two) references airport(IATA)
		on delete cascade
);

create table mile_count	(
	customer_ID uuid,
	code        char(2),
	count       integer check (count >= 0),
	
	primary key (customer_ID, code),
	foreign key (customer_ID)  references customer(customer_ID)
		on delete cascade,
	foreign key (code)         references airline(code)
		on delete cascade
);

create table booking (	
	booking_ID  uuid default uuid_generate_v4(),
	customer_ID uuid,
	card_number char(16),

	primary key (booking_ID),
	foreign key (customer_ID, card_number) references customer_card(customer_ID, card_number)
		on delete cascade
);

create table booking_order (
	order_ID    uuid,
	booking_ID  uuid,
	date        date,
	code        char(2),
	flight_num  integer,
	price       numberic(8, 2) check (price >= 0),
	class	      varchar(10) check (class in ('first', 'economy')),
	
	primary key (order_id, booking_ID, code, flight_num, date),
	foreign key (booking_ID) references booking(booking_ID)
		on delete cascade,
	foreign key (code, flight_num, date) references flight(code, flight_num, date)
		on delete cascade
);

create index routine_list  on flight_routine (code, flight_num, airport_depart, airport_arrive);
create index flight_list   on flight         (code, flight_num, date);
create index distance_list on distance       (airport_one, airport_two);